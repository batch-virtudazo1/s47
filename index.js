console.log("Hi, A!");

// show posts
const showPosts = (posts) => {
	console.log(posts);
	let postEntries = '';

	posts.forEach((post) => {
		console.log(post);
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})
	document.querySelector('#div-post-entries').innerHTML = postEntries;
};

/*
	fetch('url', options)
	.then - ano na sunod kong gagawin
*/
fetch('https://jsonplaceholder.typicode.com/posts').then(res => res.json())
.then(data => showPosts(data));

// Add Post
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {

		// like in postman structure/express
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: {
			'Content-Type' : 'application/json'
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);
		alert('Post successfully added');

		// to clear out the fields
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	})
});

// Edit Post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	document.querySelector('#btn-submit-update').removeAttribute('disabled');
};

// Update Post
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		body: JSON.stringify({

			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {
			'Content-Type' : 'application/json'
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);
		alert("Post succefully updated");
		// to clear out the fields
		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;

		document.querySelector('#btn-submit-update').setAttribute('disabled', true);
		//document.querySelector('#btn-submit-update').disabled=true;
		
	})
})

// Delete Post
const deletePost = (id) => {
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'DELETE',
		headers: {
			'Content-Type' : 'application/json'
		}
	})
	.then(res => res.json())
	.then(dataDeleted => {
		//console.log(dataDeleted)
		let deleteItem = document.querySelector(`#post-${id}`)
		console.log(deleteItem);
		alert("Item successfully deleted")
		deleteItem.remove();
		})
}